import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiDataService {

  apiKey = '3a5b464439a6479cbc8087146192bae6';

  constructor(private http: HttpClient) { }

  getArticlesForSelectedCategory(selectedCategory = ''): Observable<any> {
    let params = new HttpParams().set('apiKey', this.apiKey);

    if(selectedCategory !== '') {
      params = params.set('category', selectedCategory);
    }

    return this.http.get('https://newsapi.org/v2/sources', { params })
    .pipe(
      catchError(err => {
        return throwError(err)
      })
    );
  }



  searchBy(value: any): Observable<any> {
    let params = new HttpParams().set('apiKey', this.apiKey);
    params = params.set('q', value);
    return this.http.get(`https://newsapi.org/v2/everything?q=${value}&apiKey=${this.apiKey}`)
    // return this.http.get('https://newsapi.org/v2/everything', { params })
    .pipe(
      catchError(err => {
        return throwError(err)
      })
    );
  }
}
