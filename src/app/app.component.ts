import { Component, OnInit } from '@angular/core';
import { ApiDataService } from './api-data.service';
import { Source, ArticlesData } from './articles';
import { debounceTime, startWith, distinctUntilChanged } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  data: ArticlesData = {};
  categoryControl: FormControl = new FormControl('');
  searchTerm: FormControl = new FormControl('');
  listArticles!: Array<Source>;
  filterBy!: string;

  categories = [
    { key: '', value: 'All'},
    { key: 'general', value: 'General'},
    { key: 'business', value: 'Business'},
    { key: 'entertainment', value: 'Entertainment'},
    { key: 'health', value: 'Health'},
    { key: 'science', value: 'Science'},
    { key: 'technology', value: 'Technology'}
  ];

  constructor(private apiDataService: ApiDataService, private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    // filer by category
    this.categoryControl.valueChanges
    .pipe(startWith(''))
    .subscribe((category) => {
      this.getArticles(category);
    });

    // search input from backend
    this.searchTerm.valueChanges
    .pipe(debounceTime(1000), distinctUntilChanged())
    .subscribe((term) => {
      console.log(term);
      if(term == '')
        this.getArticles();
      else
        this.apiDataService.searchBy(term)
        .subscribe((val) => {
          this.listArticles = val.articles;
        });
    });
  }

  getArticles(val?: string) {
    this.apiDataService.getArticlesForSelectedCategory(val).subscribe(data => {
      this.listArticles = data.sources;
      this.data = data;
    });
  }

  // search on frontend
  filter(): void {
    this.listArticles = this.data.sources?.filter(
      (article: Source) => article.name?.toLocaleLowerCase().includes(this.filterBy.trim().toLocaleLowerCase())) || [];
  }



  onScroll() {
    const length = this.listArticles.length;

    setTimeout(() =>{
      const p: any = ' '.repeat(10).split('').map((s, i) => i + 1 + length);
      while(p.length) this.listArticles.push(p.shift());
    }, 1000)
  }
}

